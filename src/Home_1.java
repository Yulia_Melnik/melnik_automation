import java.util.Arrays;

public class Home1 {
    public static void main(String[] args) {
        point1();
        point2();
        point34();
        point5();
        point6();
        point7();
        point8();
    }

    private static void point1() {
        int[] masive = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println("Before" + Arrays.toString(masive));
        for (int i = 0; i < masive.length; i++) {
            masive[i] = masive[i] + (i / 2 * 3);
        }
        System.out.println("After" + Arrays.toString(masive));
    }

    private static void point2() {
        float[] masive1 = {1.1f, 1.2f, 1.3f, 1.4f, 1.5f};
    }

    private static void point34() {
        byte[] masive2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, (byte) 3333};
        System.out.println("After" + Arrays.toString(masive2));
    }

    private static void point5() {
        byte i = 127;
        int a = Integer.MAX_VALUE;
        // long b = Long.MAX_VALUE;
        long b = Integer.MIN_VALUE - 1;

        int value = 0;
        value = i;
        System.out.println("Before: " + i + " After: " + value);
        value = a;
        System.out.println("Before: " + a + " After: " + value);
        value = (int) b;
        System.out.println("Before: " + b + " After: " + value);
    }

    private static void point6() {
        int[][] masive3 = {{0, 1, 2}, {10, 11, 12}};
        int z = 0;
        System.out.println(masive3[0][1]);
    }

    private static void point7() {
        int[] masive = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        String s = "";
        String r = "";
        for (int i1 : masive) {
            r += i1 + ";";
        }
        for (int i = 0; i < masive.length; i++) {
            s += masive[i] + ";";
        }
        String d = masive[0] + ";" + masive[1] + ";" +
                masive[2] + ";" + masive[3] + ";" +
                masive[4] + ";" + masive[5] + ";" +
                masive[6] + ";" + masive[7] + ";" +
                masive[8] + ";" + masive[9] + ";";
        System.out.println("R:" + r);
        System.out.println("D:" + d);
        System.out.println("S:" + s);
    }

    private static void point8() {
/       String i = "Я хочу переносы строк";
        String all = i.replaceAll(" ", "\n");
        System.out.println (all);
        String substring = all.substring(0,1) + "\n" + all.substring(2,6)+ "\n" + all.substring(7,15)+ "\n" + all.substring(16, i.length());


        System.out.println("2: \n" +substring);
    }
}
